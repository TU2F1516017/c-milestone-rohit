Q.1 A program to convert weight entered in pounds(lbs) to kilogram(kg)

code:
#include<stdio.h>
int main()
{
float pounds,kg,ptokg=0.453592;
printf("ENTER WEIGHT VALUE IN POUNDS = ");
scanf("%f",&pounds);
kg=ptokg*pounds;
printf("THE CONVERSION OF WEIGHT FROM POUNDS TO KILOGRAM IS = %f",kg);
return 0;
}
 
output:

ENTER WEIGHT VALUE IN POUNDS = 52
THE CONVERSION OF WEIGHT FROM POUNDS TO KILOGRAM IS = 23.586784

Q.2 A program to convert length entered in inches to centimeters(cm)

code:
#include<stdio.h>
int main()
{
float inch,cm,itoc=2.54;
printf("ENTER LENGTH VALUE IN INCH = ");
scanf("%f",&inch);
cm=itoc*inch;
printf("THE CONVERSION OF LENGTH FROM INCH TO CENTIMETER IS = %f",cm);
return 0;
}
 
output:
ENTER LENGTH VALUE IN INCH = 60
THE CONVERSION OF LENGTH FROM INCH TO CENTIMETER IS = 152.399994

Q.3 A program to convert temperature in Farenheit(F) to Celcius(C)

code:
#include<stdio.h>
int main()
{
float fl,c;
printf("ENTER TEMP VALUE IN FERENHEIT = ");
scanf("%f",&fl);
c=((fl-32)*5/9);
printf("THE CONVERSION OF TEMP FROM FERENHEIT TO CELCIUS IS = %f",c);
return 0;
}
  
output:
ENTER TEMP VALUE IN FERENHEIT = 100
THE CONVERSION OF TEMP FROM FERENHEIT TO CELCIUS IS = 37.777779

Q.4 A program to calculate the Area of a Circle

code:
#include<stdio.h>
int main()
{
float r,a;
printf("ENTER RADIUS OF A CIRCLE = ");
scanf("%f",&r);
a=r*r*3.14;
printf("THE AREA OF A CIRCLE IS = %f",a);
return 0;
}

output:
ENTER RADIUS OF A CIRCLE = 6
THE AREA OF A CIRCLE IS = 113.040000


Q.5 A program to calculate the Area of a Rectangle

code:
#include<stdio.h>
int main()
{
float l,b;
printf("ENTER LENGTH OF A RECTANGLE = ");
scanf("%f",&l);
printf("ENTER BREADTH OF A RECTANGLE = ");
scanf("%f",&b);
printf("THE AREA OF A RECTANGLE IS = %f",l*b);
return 0;
}

output:
ENTER LENGTH OF A RECTANGLE = 5
ENTER BREADTH OF A RECTANGLE = 6
THE AREA OF A RECTANGLE IS = 30.000000

Q.6 A program to calculate the Area of a Triangle

code:
#include<stdio.h>
int main()
{
float b,h;
printf("ENTER BASE OF A TRIANGLE = ");
scanf("%f",&b);
printf("ENTER HEIGHT OF A TRIANGLE = ");
scanf("%f",&h);
printf("THE AREA OF A TRIANGLE IS = %f",b*h*0.5);
return 0;
}

output:
ENTER BASE OF A TRIANGLE = 4
ENTER HEIGHT OF A TRIANGLE = 3
THE AREA OF A TRIANGLE IS = 6.000000